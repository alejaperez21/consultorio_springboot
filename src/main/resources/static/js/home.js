// JavaScript source code
angular.module('consultorio',[])
.controller('filasCuentas',function($scope,$http){
    $scope.consultar = function(){
        if($scope.cedula == undefined || $scope.cedula == null){
            $scope.cedula = 0;
        }
        // GET PUT POST DELETE
        $http.get("/usuario?cedula="+$scope.cedula).success(function(data){
            $scope.filas = [
                //{ id: 1, numcuenta: 1351651} ejemplo
                data
            ];
        });
    };
});


