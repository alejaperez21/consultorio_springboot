package com.mycompamy.Consultorio.logica;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author May Perez
 */
@Component("usuario") // eS la declaración del SprintBootsepa que el modelo hace referencia al modelo While
public class Usuario {
    //Programacion oruientada a objetos

    @Autowired  //Permite la conexion del Jdbc Template a la base de datos
    transient JdbcTemplate jdbcTemplate; // ES equivalente al Statement de la clase Conexión
    // transient permite que el JSON ignore el campo JdbTemplate, si no lo hacemos lanzará un error de campos repetidos por el @Autowired

    //Atributos
    private int idusuarios;
    private String nombreUusario;
    private String tipoUsuario;
    private String cedula;
    private String fechaCreacion;
    private String fechaModificacion;
    private String fechaUltimoIngreso;
    private String direccion;
    private String localidad;
    private String ciudad;
    private String genero;
    private String fechaNacimiento;
    private String observaciones;
    private String correo;
    private String celular;
    private String contrasena;
    private String fraseRecordacion;

    //Cosntructor           
    public Usuario(int idusuarios, String nombreUusario, String tipoUsuario, String cedula, String fechaCreacion,
            String fechaModificacion, String fechaUltimoIngreso, String direccion, String localidad, String ciudad,
            String genero, String fechaNacimiento, String observaciones, String correo, String celular, String contrasena,
            String fraseRecordacion) {

        this.idusuarios = idusuarios;
        this.nombreUusario = nombreUusario;
        this.tipoUsuario = tipoUsuario;
        this.cedula = cedula;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.fechaUltimoIngreso = fechaUltimoIngreso;
        this.direccion = direccion;
        this.localidad = localidad;
        this.ciudad = ciudad;
        this.genero = genero;
        this.fechaNacimiento = fechaNacimiento;
        this.observaciones = observaciones;
        this.correo = correo;
        this.celular = celular;
        this.contrasena = contrasena;
        this.fraseRecordacion = fraseRecordacion;

    }

    public Usuario() {

    }

    //Metodos
    public int getIdusuarios() {
        return idusuarios;
    }

    public void setIdusuarios(int idusuarios) {
        this.idusuarios = idusuarios;
    }

    public String getNombreUusario() {
        return nombreUusario;
    }

    public void setNombreUusario(String nombreUusario) {
        this.nombreUusario = nombreUusario;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getFechaUltimoIngreso() {
        return fechaUltimoIngreso;
    }

    public void setFechaUltimoIngreso(String fechaUltimoIngreso) {
        this.fechaUltimoIngreso = fechaUltimoIngreso;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getFraseRecordacion() {
        return fraseRecordacion;
    }

    public void setFraseRecordacion(String fraseRecordacion) {
        this.fraseRecordacion = fraseRecordacion;
    }
    

    //CRUD
    public String guardar() throws ClassNotFoundException, SQLException {
        //CRUD -C
        //tipo del dato (Conexion) y el nombre del dato (con)

        String sql = "INSERT INTO usuarios(idusuarios,nombreUusario,tipoUsuario,cedula,fechaCreacion,fechaModificacion,fechaUltimoIngreso,direccion,localidad,ciudad,genero,fechaNacimiento,observaciones,correo,celular,contrasena,fraseRecordacion) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        return sql;
    }

    public boolean consultar() throws SQLException, ClassNotFoundException {
        // CRUD - R  Read = Leer, Consultar

        String sql = "SELECT idusuarios,nombreUusario,tipoUsuario,cedula,fechaCreacion,fechaModificacion,fechaUltimoIngreso,direccion,localidad,ciudad,genero,fechaNacimiento,observaciones,correo,celular,contrasena,fraseRecordacion FROM usuarios WHERE idusuarios = ?";
        // Una funcion puede ser escrita de dos formas. La larga:
        // public String funcion(Tipo1 param1, Tipo2 param2){
        //  return unString;
        // }
        // Una forma "inline" osea corta:
        // (param1, param2) -> unString;
        List<Usuario> usuarios = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Usuario(
                        rs.getInt("idusuarios"),
                        rs.getString("nombreUusario"),
                        rs.getString("tipoUsuario"),
                        rs.getString("cedula"),
                        rs.getString("fechaCreacion"),
                        rs.getString("fechaModificacion"),
                        rs.getString("fechaUltimoIngreso"),
                        rs.getString("direccion"),
                        rs.getString("localidad"),
                        rs.getString("ciudad"),
                        rs.getString("genero"),
                        rs.getString("fechaNacimiento"),
                        rs.getString("observaciones"),
                        rs.getString("correo"),
                        rs.getString("celular"),
                        rs.getString("contrasena"),
                        rs.getString("fraseRecordacion")
                ), new Object[]{this.getIdusuarios()});
        if (usuarios != null && usuarios.size() > 0) {
            this.setIdusuarios(usuarios.get(0).getIdusuarios());
            this.setNombreUusario(usuarios.get(0).getNombreUusario());
            this.setTipoUsuario(usuarios.get(0).getTipoUsuario());
            this.setCedula(usuarios.get(0).getCedula());
            this.setFechaCreacion(usuarios.get(0).getFechaCreacion());
            this.setFechaModificacion(usuarios.get(0).getFechaModificacion());
            this.setFechaUltimoIngreso(usuarios.get(0).getFechaUltimoIngreso());
            this.setDireccion(usuarios.get(0).getDireccion());
            this.setLocalidad(usuarios.get(0).getLocalidad());
            this.setCiudad(usuarios.get(0).getCiudad());
            this.setGenero(usuarios.get(0).getGenero());
            this.setFechaNacimiento(usuarios.get(0).getFechaNacimiento());
            this.setObservaciones(usuarios.get(0).getObservaciones());
            this.setCorreo(usuarios.get(0).getCorreo());
            this.setCelular(usuarios.get(0).getCelular());
            this.setContrasena(usuarios.get(0).getContrasena());
            this.setFraseRecordacion(usuarios.get(0).getFraseRecordacion());

            return true;

        } else {
            return false;
        }

    }

    public String actualizar() throws SQLException, ClassNotFoundException {
        // CRUD - U  Update = Actualizar

        String sql = "UPDATE usuarios SET cedula = ?, idusuarios = ?, nombreUusario = ?, tipoUsuario = ?, cedula = ?, fechaCreacion = ?, fechaModificacion = ?, fechaUltimoIngreso = ?, direccion  = ?, localidad = ?, ciudad = ?, genero = ?, fechaNacimiento = ?, observaciones  = ?, correo = ?, celular = ?, contrasena = ?, fraseRecordacion = ?  WHERE idusuarios = ?";
        return sql;
    }

    public String borrar() throws ClassNotFoundException, SQLException {
        // CRUD - D  Delete = Borrar

        String sql = "DELETE FROM usuarios WHERE idusuarios = ?";
        return sql;

    }
    
    @Override
    public String toString() {
        return "Usuario{" + "idusuarios = " + idusuarios
                + ",nombreUusario = " + nombreUusario
                + ",tipoUsuario =" + tipoUsuario
                + ",cedula =" + cedula
                + ",fechaCreacion =" + fechaCreacion
                + ",fechaModificacion =" + fechaModificacion
                + ",fechaUltimoIngreso =" + fechaUltimoIngreso
                + ",direccion =" + direccion
                + ",localidad =" + localidad
                + ",ciudad =" + ciudad
                + ",genero =" + genero
                + ",fechaNacimiento =" + fechaNacimiento
                + ",observaciones =" + observaciones
                + ",correo =" + correo
                + ",celular =" + celular
                + ",contrasena =" + contrasena
                + ",fraseRecordacion =" + fraseRecordacion + '}';

    }

}
