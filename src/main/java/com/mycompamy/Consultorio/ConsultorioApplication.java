package com.mycompamy.Consultorio;

import com.google.gson.Gson;
import com.mycompamy.Consultorio.logica.Usuario;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication// Donde se encuentre este arroba, es el Main.
@RestController// Nos ibdica que la clase seerá una api Rest
public class ConsultorioApplication {

    @Autowired // Permite la conexion del jdbcTemplate a la base de datos
    Usuario u;
    //JdbcTemplate jdbcTemplate; // Es la instancia Jdbc Template el cual es el equivalente al statement de la conexion
    // Ventajas: manejo de excepciones controlado.
    // Evita el SQL Injection que pueden ingresar los hackers porque valida las entradas de los parametros.

    public static void main(String[] args) {
        SpringApplication.run(ConsultorioApplication.class, args);

    }

    @GetMapping("/hola") // Es la vistsa en la que nos ubicamos
    public String hola(@RequestParam(value = "name", defaultValue = "World") String name,
            @RequestParam(value = "edad", defaultValue = "30") String edad) {
        return String.format("Hello %s, you are %s old!", name, edad);

    }

    @GetMapping("/usuario")
    public String consultarUsuario(@RequestParam(value = "id", defaultValue = "1") int id) throws ClassNotFoundException, SQLException {
        u.setIdusuarios(id);
        if (u.consultar()) {
            // return String.format(u.toString());
            //return "{\"idusuarios\":\""+u.getIdusuarios()+"\",\"nombreUusuario\":\""+u.getNombreUusario()+"\"}";
            return new Gson().toJson(u);
        } else {
            //return "{\"idusuarios\":\""+id+"La cédula no tiene asociado un paciente."+"\"}";
            return new Gson().toJson(u);

        }

    }
}
